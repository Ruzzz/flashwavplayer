package
{
    import flash.display.Sprite;
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
    import flash.ui.ContextMenu;
    import flash.ui.ContextMenuItem;
    
    import view.Player;

    [SWF(width = "247", height = "47", backgroundColor = "0xFFFFFF", frameRate = "30")]
    public class WavPlayer extends Sprite
    {
        static public const DEBUG: Boolean = false;
        
        static private const VERSION: String = "Flash WavPlayer v1.0" + DEBUG ? " Debug" : "";
        
        static private const PARAM_AUTOPLAY: String = "autoplay";
        static private const PARAM_AUDIO_URL: String = "url";
        static private const PARAM_DESC: String = "desc";

        private var player: Player;
        private var autoplay: Boolean = false;
        private var audioUrl: String;
        private var desc: String;

        //
        //  ctor
        //

        public function WavPlayer()
        {
            stage.scaleMode = StageScaleMode.NO_SCALE;
            stage.align = StageAlign.TOP_LEFT;

            // Version in menu
            var menuItemVersion: ContextMenuItem = new ContextMenuItem(VERSION);
            var menu: ContextMenu = new ContextMenu();
            menu.hideBuiltInItems();
            menu.customItems.push(menuItemVersion);
            contextMenu = menu;
            
            var params: Object = root.loaderInfo.parameters;

            if (params.hasOwnProperty(PARAM_AUTOPLAY))
                autoplay = params[PARAM_AUTOPLAY] == "1";
            if (params.hasOwnProperty(PARAM_DESC)) {
                desc = params[PARAM_DESC];
                //desc = unescape(desc);
            }
            if (params.hasOwnProperty(PARAM_AUDIO_URL))
                audioUrl = params[PARAM_AUDIO_URL];
            
            player = new Player;
            addChild(player);
            
            player.load(audioUrl, desc, autoplay);
        }
    }
}
