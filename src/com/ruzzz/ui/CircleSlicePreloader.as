package com.ruzzz.ui
{
    import com.ruzzz.ui.helpers.StageHandler;
    
    import flash.display.DisplayObject;
    import flash.display.Shape;
    import flash.display.Sprite;
    import flash.events.TimerEvent;
    import flash.utils.Timer;

    public class CircleSlicePreloader extends Sprite
    {
        static private const SLICE_COUNT: int = 12;
        static private const COLOR_START: uint = 0xFFFFFF;
        static private const COLOR_END: uint = 0x292c28;
        static private const RADIUS: int = 6;
        static private const PADDING: Number = 15;

        private const holder: Sprite = new Sprite;
        private const rotateTimer: Timer = new Timer(65);
        private const onStageHandler: StageHandler =
            new StageHandler(this as DisplayObject, onAddedToStage, onRemovedFromStage);

        //
        //    constructor
        //

        public function CircleSlicePreloader()
        {
            super();
            initDraw();
            addChild(holder);
        }

        //
        //    private methods
        //

        private function onAddedToStage(): void
        {
            rotateTimer.addEventListener(TimerEvent.TIMER, onTimer);
            rotateTimer.start()
        }

        private function onRemovedFromStage(): void
        {
            rotateTimer.stop();
            rotateTimer.removeEventListener(TimerEvent.TIMER, onTimer);
        }

        private function onTimer(event: TimerEvent): void
        {
            holder.rotation = (holder.rotation + (360 / SLICE_COUNT)) % 360;
        }

        private function initDraw(): void
        {
            var i: int = SLICE_COUNT;
            var degrees: int = 360 / SLICE_COUNT;
            while (i--) {
                var slice: Shape = getSlice(i);
                slice.alpha = Math.max(0.2, 1 - (0.1 * i));
                var radianAngle: Number = (degrees * i) * Math.PI / 180;
                slice.rotation = -degrees * i;
                slice.x = Math.sin(radianAngle) * RADIUS;
                slice.y = Math.cos(radianAngle) * RADIUS;
                holder.addChild(slice);
            }

            // bg
            /*var rc: Rectangle = holder.getRect(this);
            graphics.lineStyle(0, 0, 0);
            graphics.beginFill(0xFFFFFF, 1);
            graphics.drawRoundRect(rc.x - PADDING, rc.y - PADDING, rc.width + PADDING + PADDING, rc.height + PADDING + PADDING, 20, 20);
            graphics.endFill();*/
        }

        private function getSlice(step: int): Shape
        {
            var slice: Shape = new Shape();
            slice.graphics.beginFill(0x666666);
            slice.graphics.drawRoundRect(-1, 0, 2, 6, 12, 12);
            slice.graphics.endFill();
            return slice;
        }
    }
}
