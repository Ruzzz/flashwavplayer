package com.ruzzz.ui
{
	import com.ruzzz.ui.helpers.StageHandler;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;

	public class SpriteEx extends Sprite
	{
		private const onStageHandler_: StageHandler =
			new StageHandler(this as DisplayObject, onAddedToStage, OnRemovedFromStage);

		protected var isOnStage: Boolean = false;

		//----------------------------------------------------------------------
		//
		//	ctor
		//
		//----------------------------------------------------------------------

		public function SpriteEx()
		{
			//
		}

		//----------------------------------------------------------------------
		//
		//	public helpers
		//
		//----------------------------------------------------------------------

		public function removeAllChildren(): void
		{
			removeChildren();
		}

		//----------------------------------------------------------------------
		//
		//	protected methods
		//
		//----------------------------------------------------------------------

		protected function invalidateProperties(): void
		{
			if (isOnStage)
				commitProperties();
		}

		//----------------------------------------------------------------------
		//
		//	abstract methods
		//
		//----------------------------------------------------------------------

		protected function init(): void
		{
			//
		}

		protected function commitProperties() :void
		{
			//
		}

		protected function cleanUp(): void
		{
			//
		}

		//----------------------------------------------------------------------
		//
		//	callbacks
		//
		//----------------------------------------------------------------------

		private function onAddedToStage(): void
		{
			isOnStage = true;
			init();
			invalidateProperties();
		}

		private function OnRemovedFromStage(): void
		{
			cleanUp();
			isOnStage = false;
		}
	}
}

