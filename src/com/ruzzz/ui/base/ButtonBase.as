package com.ruzzz.ui.base
{
	import com.ruzzz.ui.SpriteEx;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	// [Event(name="click", type="flash.events.MouseEvent")]
	[Event(name="change", type="flash.events.Event")]
	public class ButtonBase extends SpriteEx implements IToggle
	{
		private const STATE_UP: int = 0;
		private const STATE_DOWN: int = 1;
		private const STATE_OVER: int = 2;
		private const STATE_OVER_DOWN: int = 3;
		private const STATE_INACTIVE_UP: int = 4;
		private const STATE_INACTIVE_DOWN: int = 5;

		private const states_: Vector.<DisplayObject> = new Vector.<DisplayObject>(6);  // [STATE_ID] = DisplayObject
		private const hitAreaHolder_: Sprite = new Sprite;

		private var hitTestState_: DisplayObject;
		private var isToggle_: Boolean;
		private var autoToggle_: Boolean;

		private var currentStateId_: int = -1;
		private var isPushed_: Boolean = false;
		private var isOver_: Boolean = false;

		//----------------------------------------------------------------------
		//
		//	ctor
		//
		//----------------------------------------------------------------------

		public function ButtonBase(skinData: ButtonSkinVO,
								   isToggle: Boolean = false,
								   autoToggle: Boolean = false)  // If true - for PushButton or CheckBox, else - for RadioButton
		{
			super();

			states_[STATE_UP] = skinData.upState;
			states_[STATE_DOWN] = skinData.downState;
			states_[STATE_OVER] = skinData.overState != null ? skinData.overState : skinData.upState;
			states_[STATE_OVER_DOWN] = skinData.overDownState != null ? skinData.overDownState : skinData.downState;
			states_[STATE_INACTIVE_UP] = skinData.inactiveUpState != null ? skinData.inactiveUpState : skinData.upState;
			states_[STATE_INACTIVE_DOWN] = skinData.inactiveDownState != null ? skinData.inactiveDownState : skinData.downState;

			this.hitTestState_ = skinData.hitTestState;
			this.isToggle_ = isToggle;
			this.autoToggle_ = autoToggle;

			initInternal();
		}

		//----------------------------------------------------------------------
		//
		//	public
		//
		//----------------------------------------------------------------------

        private var toolTip_: String;
        public function get toolTip(): String
        {
            return toolTip_;
        }
        public function set toolTip(value: String): void
        {
            toolTip_ = value;
        }
        
		// Uses only if toggle == true
		private var checked_: Boolean;
		private var checkedChanged: Boolean;
		public function get checked(): Boolean
		{
			return isToggle_ ? checked_ : false;
		}
		public function set checked(value: Boolean): void
		{
			if (isToggle_ && checked_ != value) {
				checked_ = value;
				checkedChanged = true;
				invalidateProperties();
                if (!changeQuiet_)
				    dispatchEvent(new Event(Event.CHANGE));
			}
		}

        private var changeQuiet_: Boolean = false;
        
        public function setChecked(value: Boolean): void
        {
            changeQuiet_ = true;
            checked = value;
            changeQuiet_ = false;
        }
        
		private var enable_: Boolean = false;
		private var enableChanged: Boolean;
		public function get enable(): Boolean
		{
			return enable_;
		}
		public function set enable(value: Boolean): void
		{
			if (enable_ != value) {
				enable_ = value;
				enableChanged = true;
				invalidateProperties();
                mouseEnabled = enable_;
			}
		}

		//----------------------------------------------------------------------
		//
		//	internal
		//
		//----------------------------------------------------------------------

		internal function forceDownState(): void
		{
			mouseDownHandler(null);
		}

		//----------------------------------------------------------------------
		//
		//	private
		//
		//----------------------------------------------------------------------

		private function initInternal(): void
		{
			mouseChildren = false;
			buttonMode = true;
			enable = true;  // Also force to call updateState()

			for (var i: int = states_.length - 1; i >= 0; --i)
				if (!contains(states_[i]))
					addChild(states_[i]);

			hitAreaHolder_.addChild(hitTestState_);
			addChild(hitAreaHolder_);
			hitArea = hitAreaHolder_;
		}

		private function setState(stateId: int): void
		{
			if (currentStateId_ != stateId) {
				var newState: DisplayObject = states_[stateId];
				for (var i: int = states_.length - 1; i >= 0; --i) {
					var state: DisplayObject = states_[i];
					if (i != stateId && newState != state)
						state.visible = false;
				}
				newState.visible = true;
				currentStateId_ = stateId;
			}
		}

		private function updateState(): void
		{
			var id: int = STATE_UP;
			if (isPushed_)
				++id;
			if (!enable_) {
				id += 4;
                isOver_ = false;
            } else if (isOver_)
			//else if (enable_ && isOver_) // hack
				id += 2;
			setState(id);
		}

		//----------------------------------------------------------------------
		//
		//	overrides
		//
		//----------------------------------------------------------------------

		override protected function init(): void
		{
			// priority > 0 or parent gets the first
			addEventListener(MouseEvent.CLICK, mouseClickHandler, false, 1000);
			addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler, false, 1000);
			addEventListener(MouseEvent.ROLL_OVER, mouseOverHandler, false, 1000);
			addEventListener(MouseEvent.ROLL_OUT, mouseOutHandler, false, 1000);
		}

		override protected function commitProperties() :void
		{
			if (enableChanged) {
				enableChanged = false;
				if (!checkedChanged)
					updateState();
			}

			if (checkedChanged) {
				checkedChanged = false;
				isPushed_ = checked;
				updateState();
			}
		}

		override protected function cleanUp(): void
		{
			removeEventListener(MouseEvent.CLICK, mouseClickHandler);
			removeEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
			removeEventListener(MouseEvent.ROLL_OVER, mouseOverHandler);
			removeEventListener(MouseEvent.ROLL_OUT, mouseOutHandler);

			removeEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
			isOver_ = false;
			if (!isToggle_)
				isPushed_ = false;
			updateState();
		}

		//----------------------------------------------------------------------
		//
		//	event handlers
		//
		//----------------------------------------------------------------------

		private var isMouseDown: Boolean;

		private function mouseClickHandler(e: MouseEvent): void
		{
			if (!enable_)
				e.stopImmediatePropagation();
		}

		private function mouseDownHandler(e: MouseEvent): void
		{
			isMouseDown = true;
			if (enable_) {
				addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler, false, 1000);
				isPushed_ = true;
				updateState();
			}
		}

		private function mouseUpHandler(e: MouseEvent): void
		{
			isMouseDown = false;
			removeEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
			if (isToggle_) {
				checked = (autoToggle_) ? (!checked) : true;
			} else {
				isPushed_ = false;
				updateState();
			}
		}

		private function mouseOverHandler(e: MouseEvent): void
		{
			if (enable_) {
				isOver_ = true;
				updateState();
			}
		}

		private function mouseOutHandler(e: MouseEvent): void
		{
			if (enable_) {
				isOver_ = false;
				updateState();

				if (isMouseDown) {  // Stage inaccessible :(
					isMouseDown = false;
					removeEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
					if (isToggle_)
						isPushed_ = checked;
					else
						isPushed_ = false;
					updateState();
				}
			}
		}
	}
}