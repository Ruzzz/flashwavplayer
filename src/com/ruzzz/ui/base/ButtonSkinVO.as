package com.ruzzz.ui.base
{
	import flash.display.DisplayObject;

	public final class ButtonSkinVO
	{
		public var upState: DisplayObject;
		public var downState: DisplayObject;
		public var hitTestState: DisplayObject;
		public var overState: DisplayObject;
		public var overDownState: DisplayObject;
		public var inactiveUpState: DisplayObject;
		public var inactiveDownState: DisplayObject;
		
		public function ButtonSkinVO(upState: DisplayObject,
									 downState: DisplayObject,
									 hitTestState: DisplayObject,
									 overState: DisplayObject = null,
									 overDownState: DisplayObject = null,
									 inactiveUpState: DisplayObject = null,
									 inactiveDownState: DisplayObject = null)
		{
			this.upState = upState;
			this.downState = downState;
			this.hitTestState = hitTestState;
			this.overState = overState;
			this.overDownState = overDownState;
			this.inactiveUpState = inactiveUpState;
			this.inactiveDownState = inactiveDownState;
		}
	}
}