package com.ruzzz.ui.base
{
	public final class ButtonType
	{
		static public const BUTTON: String = "button";
		static public const CHECKBOX: String = "checkbox";
		static public const RADIOBUTTON: String = "radiobutton";
	}
}