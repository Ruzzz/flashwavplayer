package com.ruzzz.ui.base
{
	import flash.display.DisplayObject;

	public final class ButtonsFactory implements IButtonsFactory
	{
		//----------------------------------------------------------------------
		//
		//	ctor
		//
		//----------------------------------------------------------------------
		
		public function ButtonsFactory(defaultRenderer: IButtonRenderer = null)
		{
			defaultRenderer_ = defaultRenderer;
		}
		
		//----------------------------------------------------------------------
		//
		//	fields
		//
		//----------------------------------------------------------------------
		
		private var defaultRenderer_: IButtonRenderer;
		public function get defaultRenderer(): IButtonRenderer
		{
			return defaultRenderer_;
		}

		public function set defaultRenderer(value: IButtonRenderer): void
		{
			if (defaultRenderer_ != value)
				defaultRenderer_ = value;
		}
		
		//----------------------------------------------------------------------
		//
		//	methods
		//
		//----------------------------------------------------------------------
		
		public function create(buttonType: String, rendererData: Object, renderer: IButtonRenderer = null): DisplayObject
		{
			var isToggle: Boolean;
			var autoToggle: Boolean;
			
			switch (buttonType) {
				case ButtonType.CHECKBOX:
					isToggle = true;
					autoToggle = true;
					break;
					
				case ButtonType.RADIOBUTTON:
					isToggle = true;
					autoToggle = false;
					break;
					
				case ButtonType.BUTTON:
				default:
					isToggle = false;
					autoToggle = false;
					break;
			}
			
			var renderer_: IButtonRenderer = renderer != null ? renderer : defaultRenderer_;
			return new ButtonBase(renderer_.getSkinData(rendererData), isToggle, autoToggle);
		}
		
		static public function createButton(buttonType: String, rendererData: Object, renderer: IButtonRenderer): DisplayObject
		{
			var isToggle: Boolean;
			var autoToggle: Boolean;
			
			switch (buttonType) {
				case ButtonType.CHECKBOX:
					isToggle = true;
					autoToggle = true;
					break;
				
				case ButtonType.RADIOBUTTON:
					isToggle = true;
					autoToggle = false;
					break;
				
				case ButtonType.BUTTON:
				default:
					isToggle = false;
					autoToggle = false;
					break;
			}
			
			return new ButtonBase(renderer.getSkinData(rendererData), isToggle, autoToggle);
		}
	}
}