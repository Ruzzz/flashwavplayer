package com.ruzzz.ui.base
{
	public interface IButtonRenderer
	{
		function getSkinData(rendererData: Object = null): ButtonSkinVO;
	}
}