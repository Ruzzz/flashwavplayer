package com.ruzzz.ui.base
{
	import flash.display.DisplayObject;

	public interface IButtonsFactory
	{
		function get defaultRenderer(): IButtonRenderer;
		function set defaultRenderer(value: IButtonRenderer): void;
		function create(buttonType: String, rendererData: Object, renderer: IButtonRenderer = null): DisplayObject;
	}
}