package com.ruzzz.ui.base
{

	public interface ISliderRenderer
	{
		function getSkinData(): SliderSkinVO;
	}
}