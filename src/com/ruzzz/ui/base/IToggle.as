package com.ruzzz.ui.base
{
	import flash.events.IEventDispatcher;

	/**
	 * Must:
	 *   Dispatch Event.CHANGE
	 */
	public interface IToggle extends IEventDispatcher
	{
		function get enable(): Boolean;
		function set enable(value: Boolean): void;
			
		function get checked(): Boolean;
		function set checked(value: Boolean): void;
	}
}