package com.ruzzz.ui.base
{
	import com.ruzzz.ui.SpriteEx;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;

	[Event(name="change", type="flash.events.Event")]
	public class SliderBase extends SpriteEx
	{
		static public const HORIZONTAL_ORIENTATION: uint = 0;
		static public const VERTICAL_ORIENTATION: uint = 1;

		//----------------------------------------------------------------------
		//
		//	Private fields
		//
		//----------------------------------------------------------------------

		private const STATE_UP: int = 0;
		private const STATE_DOWN: int = 1;
		private const STATE_OVER: int = 2;

		private const hotArea_: Sprite = new Sprite;
		private const holder_: Sprite = new Sprite;

		private var backgroundHolder_: Sprite = null;
		private var dragger_: ButtonBase;
		private var isDrag_: Boolean;

		private var minDraggerPos_: Number;
		private var maxDraggerPos_: Number;
		private var maxValue_: Number;
		private var minValue_: Number;
		private var hotCoord_: Number = 0;
		private var background_: DisplayObject;
		private var hotAreaBounds_: Rectangle;
		private var changeHandler_: Function;
		private var orientation_: uint;

		//----------------------------------------------------------------------
		//
		//	ctor
		//
		//----------------------------------------------------------------------

		public function SliderBase(skinData: SliderSkinVO,
								   changeHandler: Function = null,
								   minValue: Number = 0,
								   maxValue: Number = 1,
								   orientation: uint = HORIZONTAL_ORIENTATION)
		{
			super();
			orientation_ = orientation;

			maxValue_ = maxValue;
			minValue_ = minValue;

			changeHandler_ = changeHandler;
			hotAreaBounds_ = skinData.hotAreaBounds;
			background_ = skinData.backgound;

			var draggerSkinData: ButtonSkinVO = new ButtonSkinVO(
				skinData.dragger,
				skinData.draggerPressed != null ? skinData.draggerPressed : skinData.dragger,
				skinData.draggerHitArea != null ? skinData.draggerHitArea : skinData.dragger,
				skinData.draggerOver
			);
			this.dragger_ = new ButtonBase(draggerSkinData);

			initInternal();
		}

		//----------------------------------------------------------------------
		//
		//	Public fields
		//
		//----------------------------------------------------------------------
        
        private var toolTip_: String;
        public function get toolTip(): String
        {
            return toolTip_;
        }
        public function set toolTip(value: String): void
        {
            toolTip_ = value;
        }
        
		private var value_: Number;
		public function get value(): Number
		{
			return value_;
		}
		public function set value(newValue: Number): void
		{
			if (value_ != newValue) {
				var pos: Number;
				if (isHorizontal)
					pos = remap(newValue, minValue_, maxValue_, minDraggerPos_, maxDraggerPos_);
				else
					pos = remap(newValue, minValue_, maxValue_, maxDraggerPos_, minDraggerPos_);
				moveDraggerTo(pos);
			}
		}
		private function updateValue(): void
		{
			if (isHorizontal)
				value_ = remap(dragger_.x, minDraggerPos_, maxDraggerPos_, minValue_, maxValue_);
			else
				value_ = remap(dragger_.y, minDraggerPos_, maxDraggerPos_, maxValue_, minValue_);
		}

		public function get isHorizontal(): Boolean
		{
			return orientation_ == HORIZONTAL_ORIENTATION;
		}

		//----------------------------------------------------------------------
		//
		//	Private methods
		//
		//----------------------------------------------------------------------

		private function initInternal(): void
		{
			/*buttonMode = true;
			mouseChildren = false;*/

			if (background_) {
				backgroundHolder_ = new Sprite;
				backgroundHolder_.addChild(background_);
				backgroundHolder_.mouseEnabled = backgroundHolder_.buttonMode = true;
				backgroundHolder_.mouseChildren = false;
				holder_.addChild(backgroundHolder_);
			}

			hotArea_.graphics.clear();
			hotArea_.graphics.lineStyle(0, 0, 0);
			hotArea_.graphics.beginFill(0, 0);
			hotArea_.graphics.drawRect(hotAreaBounds_.x, hotAreaBounds_.y, hotAreaBounds_.width, hotAreaBounds_.height);
			hotArea_.graphics.endFill();
			hotArea_.mouseEnabled = hotArea_.buttonMode = true;
			holder_.addChild(hotArea_);

			if (isHorizontal) {
				minDraggerPos_ = hotAreaBounds_.left;
				maxDraggerPos_ = hotAreaBounds_.right;
				dragger_.y = hotAreaBounds_.height / 2 + hotAreaBounds_.top;
			} else {
				minDraggerPos_ = hotAreaBounds_.top;
				maxDraggerPos_ = hotAreaBounds_.bottom;
				dragger_.x = hotAreaBounds_.width / 2 + hotAreaBounds_.left;
			}
			holder_.addChild(dragger_);

			paintTransparentBackground();

			addChild(holder_);
			value = minValue_ + (maxValue_ - minValue_) / 2;
			updateValue();
		}

		private function paintTransparentBackground(): void
		{
			value = isHorizontal ? minValue_ : maxValue_;
			var rc: Rectangle = holder_.getRect(this);
			holder_.x -= rc.left;
			holder_.y -= rc.top;

			value = isHorizontal ? maxValue_ : minValue_;
			rc = holder_.getRect(this);

			graphics.lineStyle(0, 0, 0);
			graphics.beginFill(0, 0);
			graphics.drawRect(0, 0, rc.right, rc.bottom);
			graphics.endFill();
		}

		// Return True if position of dragger is changed
		private function moveDraggerTo(pos: Number): Boolean
		{
			pos = clamp(pos, minDraggerPos_, maxDraggerPos_);
			var oldPos: Number = isHorizontal ? dragger_.x : dragger_.y;
			if (pos != oldPos) {
				if (isHorizontal) {
					dragger_.x = pos;
				} else {
					dragger_.y = pos;
				}
				updateValue();
				return true;
			} else
				return false;
		}

		private function updateDraggerPos(): void
		{
			updateDraggerPosTo((isHorizontal ? mouseX - holder_.x : mouseY - holder_.y) - hotCoord_);
		}

		private function updateDraggerPosTo(value: Number, forceDownState: Boolean = true): void
		{
			if (moveDraggerTo(value)) {
				if (forceDownState)
					dragger_.forceDownState();
				if (changeHandler_ != null)
					changeHandler_();
				dispatchEvent(new Event(Event.CHANGE));
			}
		}

		private function startDraggerMoving(): void
		{
			isDrag_ = true;
			hotCoord_ = isHorizontal ? dragger_.mouseX : dragger_.mouseY;
			addEventListener(MouseEvent.MOUSE_UP, stage_mouseUpHandler);  // stage
			addEventListener(MouseEvent.MOUSE_MOVE, stage_mouseMoveHandler);
		}

		private function stopDraggerMoving(): void
		{
			isDrag_ = false;
			removeEventListener(MouseEvent.MOUSE_MOVE, stage_mouseMoveHandler);
			removeEventListener(MouseEvent.MOUSE_UP, stage_mouseUpHandler);
			hotCoord_ = 0;
		}

		//----------------------------------------------------------------------
		//
		//	overrides
		//
		//----------------------------------------------------------------------

		override protected function init(): void
		{
			if (backgroundHolder_) {
				backgroundHolder_.addEventListener(MouseEvent.MOUSE_DOWN, hotArea_mouseDownHandler);
			}
			hotArea_.addEventListener(MouseEvent.MOUSE_DOWN, hotArea_mouseDownHandler);
			dragger_.addEventListener(MouseEvent.MOUSE_DOWN, dragger_mouseDownHandler);
			addEventListener(MouseEvent.MOUSE_WHEEL, mouseWheelHandler);

			addEventListener(MouseEvent.ROLL_OUT, stage_mouseUpHandler);  // Stage inaccessible :(
		}

		override protected function cleanUp(): void
		{
			if (backgroundHolder_) {
				backgroundHolder_.removeEventListener(MouseEvent.MOUSE_DOWN, hotArea_mouseDownHandler);
			}
			hotArea_.removeEventListener(MouseEvent.MOUSE_DOWN, hotArea_mouseDownHandler);
			dragger_.removeEventListener(MouseEvent.MOUSE_DOWN, dragger_mouseDownHandler);
			removeEventListener(MouseEvent.MOUSE_WHEEL, mouseWheelHandler);

			removeEventListener(MouseEvent.ROLL_OUT, stage_mouseUpHandler);  // Stage inaccessible :(

			stopDraggerMoving();
		}

		//----------------------------------------------------------------------
		//
		//	event handlers
		//
		//----------------------------------------------------------------------

		private function mouseWheelHandler(event: MouseEvent): void
		{
			if (!isDrag_) {
				var dx: Number = (maxDraggerPos_ - minDraggerPos_) / 10;
				var pos: Number = isHorizontal ? dragger_.x : dragger_.y;
				if (event.delta > 0)
					pos += dx;
				else
					pos -= dx;
				updateDraggerPosTo(pos, false);
			}
		}

		private function dragger_mouseDownHandler(event: MouseEvent): void
		{
			startDraggerMoving();
			event.stopPropagation();
		}

		private function hotArea_mouseDownHandler(event: MouseEvent): void
		{
			updateDraggerPos();
			startDraggerMoving();
			event.stopPropagation();
		}

		private function stage_mouseMoveHandler(event: MouseEvent): void
		{
			updateDraggerPos();
		}

		private function stage_mouseUpHandler(event: MouseEvent): void
		{
			stopDraggerMoving();
		}
        
        //----------------------------------------------------------------------
        //
        //	@utils@
        //
        //----------------------------------------------------------------------
        
        static public function remap(value: Number, min: Number, max: Number,
                                     destMin: Number, destMax: Number): Number
        {
            if (max < min) {
                var t: Number = max;
                max = min;
                min = t;
            }
            var len: Number = max - min;
            var destLen: Number = destMax - destMin;
            return (value * destLen + max * destMin - min * destMax) / len;
        }
        
        static public function clamp(value: Number, min: Number, max: Number): Number
        {
            if (max < min) {
                var t: Number = max;
                max = min;
                min = t;
            }
            if (value > max)
                return max;
            if (value < min)
                return min;
            return value;
        }
	}
}
