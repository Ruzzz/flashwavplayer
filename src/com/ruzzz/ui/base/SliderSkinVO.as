package com.ruzzz.ui.base
{
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;

	public final class SliderSkinVO
	{
		public var hotAreaBounds: Rectangle;
		public var dragger: DisplayObject;
		public var draggerOver: DisplayObject;
		public var draggerPressed: DisplayObject;
		public var draggerHitArea: DisplayObject;
		public var backgound: DisplayObject;
		
		/**
		 * @param dragger Hot point must be (0, 0)
		 * @param draggerOver Hot point must be (0, 0)
		 * @param draggerPressed Hot point must be (0, 0)
		 * @param draggerHitArea Hot point must be (0, 0)
		 */
		public function SliderSkinVO(hotAreaBounds: Rectangle,
								 dragger: DisplayObject,
								 draggerOver: DisplayObject = null,
								 draggerPressed: DisplayObject = null,
								 draggerHitArea: DisplayObject = null,
								 backgound: DisplayObject = null)
		{
			this.hotAreaBounds = hotAreaBounds;
			this.dragger = dragger;
			this.draggerOver = draggerOver;
			this.draggerPressed = draggerPressed;
			this.draggerHitArea = draggerHitArea;
			this.backgound = backgound;
		}
	}
}