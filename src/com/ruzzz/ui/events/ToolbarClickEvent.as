package com.ruzzz.ui.events
{
    public class ToolbarClickEvent extends ToolbarEventBase
    {
        public function ToolbarClickEvent(action: String)
        {
            super(ACTION, action);
        }
    }
}