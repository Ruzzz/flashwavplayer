package com.ruzzz.ui.events
{
	import flash.events.Event;

	public class ToolbarEventBase extends Event
	{
		static public const ACTION: String = "action";

		public var action: String;

        protected var _data: *;
        public function get data(): * { return _data; }
        public function set data(value: *): void
        {
            if (_data == value)
                return;
            _data = value;
        }
        
		public function ToolbarEventBase(type: String, action: String, data: * = null)
		{
			super(type);
			this.action = action;
			this.data = data;
		}

		override public function clone(): Event
		{
			return new ToolbarEventBase(type, action, data);
		}
	}
}
