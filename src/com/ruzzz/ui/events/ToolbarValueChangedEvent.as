package com.ruzzz.ui.events
{
    public class ToolbarValueChangedEvent extends ToolbarEventBase
    {
        public function get value(): Number { return _data; }
        public function set value(v: Number): void { data = v; }

        public function ToolbarValueChangedEvent(action: String, value: Number)
        {
            super(ACTION, action, value);
        }
    }
}