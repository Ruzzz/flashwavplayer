package com.ruzzz.ui.helpers
{
    import flash.display.DisplayObject;
    import flash.events.Event;

    public class StageHandler
    {
        private var target: DisplayObject;
        private var onAdded: Function;
        private var onRemoved: Function;

        //
        //    ctor
        //

        public function StageHandler(target: DisplayObject,
                                     onAdded: Function = null,
                                     onRemoved: Function = null)
        {
            this.target = target;
            this.onAdded = onAdded;
            this.onRemoved = onRemoved;
            initAddedListener();
        }

        //
        //    private methods
        //

        private function initAddedListener(): void
        {
            target.removeEventListener(Event.REMOVED_FROM_STAGE, removedFromStageHandler);
            target.addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler, false, 0, true);

        }

        private function initRemovedListener(): void
        {
            target.removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
            target.addEventListener(Event.REMOVED_FROM_STAGE, removedFromStageHandler);
        }

        //
        //    event handlers
        //

        private function addedToStageHandler(event: Event): void
        {
            if (event.target == target) {
                initRemovedListener();
                if (onAdded != null)
                    onAdded();
            }
        }

        private function removedFromStageHandler(event: Event): void
        {
            if (event.target == target) {
                initAddedListener();
                if (onRemoved != null)
                    onRemoved();
            }
        }
    }
}
