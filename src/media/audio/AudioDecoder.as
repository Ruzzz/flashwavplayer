package media.audio
{
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.IOErrorEvent;
    import flash.events.ProgressEvent;
    import flash.media.Sound;
    import flash.media.SoundChannel;
    import flash.net.Socket;
    import flash.net.URLStream;
    import flash.utils.IDataInput;
    
    import media.audio.decoders.IDecoder;

    public class AudioDecoder extends EventDispatcher
    {
        static private const SAMPLE_BUFFER_SIZE: uint = 8192 / 2;
        
        private var decoder_: IDecoder;
        private var sound_: Sound;
        private var soundChannel_: SoundChannel;

        //
        //  ctor
        //
        
        public function AudioDecoder(): void
        {
            sound_ = new Sound();
        }

        //
        //  public
        //
        
        public function load(data: IDataInput,
                             decoder: Class,
                             bufferSize: uint = 65536,
                             progressEventName: String = null): void
        {
            if (progressEventName == null) {
                if (data is URLStream) {
                    progressEventName = ProgressEvent.PROGRESS;
                } else if (data is Socket) {
                    progressEventName = ProgressEvent.SOCKET_DATA;
                }
            }

            sound_.addEventListener("sampleData", samplesRequestHandler);

            decoder_ = (new decoder() as IDecoder);
            decoder_.addEventListener(Event.INIT, decoderInitHandler);
            decoder_.addEventListener(Event.COMPLETE, decoderCompleteHandler);
            decoder_.addEventListener(IOErrorEvent.IO_ERROR, decoderIOErrorHandler);

            decoder_.init(data, bufferSize, progressEventName);
        }

        public function play(): SoundChannel
        {
            soundChannel_ = sound_.play();
            return soundChannel_;
        }

        public function get sound(): Sound
        {
            return sound_;
        }

        public function get audioInfo(): AudioInfoVO
        {
            return decoder_.getAudioInfo();
        }

        public function getTotalTime(): Number
        {
            return decoder_.getTotalTime();
        }

        public function getPosition(): Number
        {
            return decoder_.getPosition();
        }

        //
        //  handlers
        //
        
        private function decoderInitHandler(event: Event): void
        {
            dispatchEvent(event);
        }

        private function decoderCompleteHandler(event: Event): void
        {
            dispatchEvent(event);
        }

        private function decoderIOErrorHandler(event: IOErrorEvent): void
        {
            dispatchEvent(event);
        }

        private function samplesRequestHandler(event: *): void
        {
            /*if (isStopping) {
                event.data.writeFloat(0);
                event.data.writeFloat(0);
            } else*/
            
            decoder_.getSamples(event.position, SAMPLE_BUFFER_SIZE, event.data);
        }

    }
}
