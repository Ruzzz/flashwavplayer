package media.audio.decoders
{
    import flash.errors.EOFError;
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.IOErrorEvent;
    import flash.utils.ByteArray;
    import flash.utils.Endian;
    import flash.utils.IDataInput;
    
    import media.audio.AudioInfoVO;
    import media.audio.filters.LowpassFilter;
    import media.data.riff.RIFFChunkInfoVO;
    import media.data.riff.RIFFParser;

    public class WAVDecoder extends EventDispatcher implements IDecoder
    {
        static private const DECODER_CLASS: String = "media.audio.decoders.WAVDecoder";
        
        private var data_: IDataInput;
        private var riffParser_: RIFFParser;
        private var audioInfo_: AudioInfoVO;
        private var nextMethod_: Function;
        private var convertMethod_: Function;
        private var isBuffering_: Boolean;
        private var bufferSize_: uint;
        private var dataSize_: uint;
        private var position_: uint;
        private var isSmallFile_: Boolean;
        
        private var last_: Number = 0;
        private var lpf_: LowpassFilter = new LowpassFilter(5200, .8);

        //
        //  ctor
        //
            
        public function WAVDecoder(): void
        {
            
        }

        //
        //  public
        //
        
        public function init(data: IDataInput, bufferSize: uint, progressEventName: String): void
        {
            data_ = data;
            riffParser_ = new RIFFParser(data);
            bufferSize_ = bufferSize;

            position_ = 0;
            dataSize_ = 0;

            nextMethod_ = parseRIFFChunk;

            if (progressEventName != null) {
                isSmallFile_ = true;
                (data as EventDispatcher).addEventListener(progressEventName, dataProgressHandler);
                (data as EventDispatcher).addEventListener(Event.COMPLETE, dataCompleteHandler);
            } else {
                nextMethod_();
            }
        }

        public function getSamples(position: Number, numSamples: uint, sampleData: ByteArray): void
        {
            trace("getSamples", position);
            
            var numBytes: uint = numSamples * audioInfo_.blockAlign / audioInfo_.sampleMultiplier;
            var dataReadSize: int;
            var sampleReadSize: int;
            var rawSampleData: ByteArray = new ByteArray();
            var i: uint;

            try {
                dataReadSize = riffParser_.streamChunkData(numBytes, rawSampleData);

                sampleReadSize = dataReadSize / audioInfo_.blockAlign;

                position_ += dataReadSize;
                rawSampleData.endian = Endian.LITTLE_ENDIAN;

                convertMethod_(sampleReadSize, rawSampleData, sampleData);

                if (!riffParser_.isDataComplete) {
                    // padd with extra 0s as needed
                    for (i = sampleReadSize * audioInfo_.sampleMultiplier; i < numSamples; ++i) {
                        sampleData.writeFloat(0);
                        sampleData.writeFloat(0);
                    }
                } else {
                    dispatchEvent(new Event(Event.COMPLETE));
                }

            } catch (error: EOFError) {
                // padd with extra 0s as needed
                for (i = 0; i < numSamples; ++i) {
                    sampleData.writeFloat(0);
                    sampleData.writeFloat(0);
                }
            }
        }

        public function getAudioInfo(): AudioInfoVO
        {
            return audioInfo_;
        }

        public function getTotalTime(): Number
        {
            return ((dataSize_ / (audioInfo_.blockAlign * audioInfo_.sampleMultiplier)) / 44100);
        }

        public function getPosition(): Number
        {
            return ((position_ / (audioInfo_.blockAlign * audioInfo_.sampleMultiplier)) / 44100);
        }

        //
        //  private
        //
        
        private function parseRIFFChunk(): void
        {
            var headerInfo: RIFFChunkInfoVO;

            try {
                headerInfo = riffParser_.readChunk();

                if (headerInfo.chunkId == "RIFF") {
                    nextMethod_ = parseRIFFSubChunk;
                    nextMethod_();
                } else
                    dispatchEvent(new IOErrorEvent(IOErrorEvent.IO_ERROR, false, false, "Bad audio format"));

            } catch (error: EOFError) { }
        }

        private function parseRIFFSubChunk(): void
        {
            var subChunkFormat: String;

            try {
                subChunkFormat = riffParser_.readSubChunk();

                if (subChunkFormat == "WAVE") {
                    nextMethod_ = locateFormatChunk;
                    nextMethod_();
                } else
                    dispatchEvent(new IOErrorEvent(IOErrorEvent.IO_ERROR, false, false, "Bad audio format"));

            } catch (error: EOFError) { }
        }

        private function locateFormatChunk(): void
        {
            var chunkInfo: RIFFChunkInfoVO;

            try {
                chunkInfo = riffParser_.readChunk();

                while (chunkInfo.chunkId != "fmt " && chunkInfo != null)
                    chunkInfo = riffParser_.readChunk();

                if (chunkInfo.chunkId == "fmt ") {
                    nextMethod_ = parseFormatChunk;
                    nextMethod_();
                } else
                    dispatchEvent(new IOErrorEvent(IOErrorEvent.IO_ERROR, false, false, "Bad audio format"));

            } catch (error: EOFError) {}
        }

        private function parseFormatChunk(): void
        {
            var chunkData: ByteArray;

            try {
                chunkData = riffParser_.readChunkData();
                chunkData.endian = Endian.LITTLE_ENDIAN;

                if (chunkData.length >= 16) {
                    audioInfo_ = new AudioInfoVO();
                    audioInfo_.decoder = DECODER_CLASS;
                    audioInfo_.format = (chunkData.readUnsignedShort() == 1 ? "PCM" : "Unknown");
                    audioInfo_.channels = chunkData.readUnsignedShort();
                    audioInfo_.sampleRate = chunkData.readUnsignedInt();
                    audioInfo_.bitRateUpper = audioInfo_.bitRateLower = chunkData.readUnsignedInt() / 8;
                    audioInfo_.blockAlign = chunkData.readUnsignedShort();
                    audioInfo_.bitsPerSample = chunkData.readUnsignedShort();
                    audioInfo_.sampleMultiplier = 44100 / audioInfo_.sampleRate;

                    trace(audioInfo_.toString());

                    if (audioInfo_.channels == 1) {
                        
                        if (audioInfo_.bitsPerSample == 8)
                            convertMethod_ = convert8BitMono;
                        else if (audioInfo_.bitsPerSample == 16)
                            convertMethod_ = convert16BitMono;
                        
                    } else if (audioInfo_.channels == 2) {
                        
                        if (audioInfo_.bitsPerSample == 8)
                            convertMethod_ = convert8BitStereo;
                        else if (audioInfo_.bitsPerSample == 16)
                            convertMethod_ = convert16BitStereo;

                    }

                    if (audioInfo_.format != "PCM" || convertMethod_ == null) {
                        dispatchEvent(new IOErrorEvent(IOErrorEvent.IO_ERROR, false, false, "Bad audio format"));
                        nextMethod_ = null;
                    } else if (audioInfo_.sampleRate != 44100 && audioInfo_.sampleRate != 22050 && audioInfo_.sampleRate != 11025) {
                        dispatchEvent(new IOErrorEvent(IOErrorEvent.IO_ERROR, false, false, "Bad sample rate"));
                        nextMethod_ = null;
                    } else {
                        nextMethod_ = locateDataChunk;
                        nextMethod_();
                    }

                } else
                    dispatchEvent(new IOErrorEvent(IOErrorEvent.IO_ERROR, false, false, "Bad audio format"));

            } catch (error: EOFError) {}
        }

        private function locateDataChunk(): void
        {
            var chunkInfo: RIFFChunkInfoVO;

            try {
                nextMethod_ = null;
                chunkInfo = riffParser_.readChunk();

                while (chunkInfo.chunkId != "data" && chunkInfo != null)
                    chunkInfo = riffParser_.readChunk();

                if (chunkInfo.chunkId == "data") {
                    dataSize_ = chunkInfo.size;
                    dispatchEvent(new Event(Event.INIT));

                    nextMethod_ = null;
                } else
                    dispatchEvent(new IOErrorEvent(IOErrorEvent.IO_ERROR, false, false, "Bad audio format"));

            } catch (error: EOFError) {
                // do nothing, wait for next attempt
            }
        }

        //
        //  convert format
        //
        
        private function convert8BitMono(numSamples: uint, inputData: ByteArray, outputData: ByteArray): void
        {
            var i: uint;
            var j: uint;
            var sample: Number;
            var sampleOriginal: Number;
            var l: uint = audioInfo_.sampleMultiplier;  // TODO
            var sampleDx: Number;
            var sampleFiltered: Number;

            for (i = 0; i < numSamples; ++i) {

                sampleOriginal = (inputData.readUnsignedByte() - 128) / 64;

                sampleDx = (sampleOriginal - last_) / l;
                sample = last_ + sampleDx;

                for (j = 0; j < l; ++j) {
                    
                    /*outputData.writeFloat(sampleOriginal);
                    outputData.writeFloat(sampleOriginal);
                    continue;*/
                    
                    sampleFiltered = lpf_.process(sample);
                    outputData.writeFloat(sampleFiltered);
                    outputData.writeFloat(sampleFiltered);
                    sample += sampleDx;
                }

                last_ = sampleOriginal;
            }
        }

        private function convert16BitMono(numSamples: uint, inputData: ByteArray, outputData: ByteArray): void
        {
            var i: uint;
            var j: uint;
            var sample: Number;

            for (i = 0; i < numSamples; ++i) {
                sample = inputData.readShort();
                sample /= 32768;

                for (j = 0; j < audioInfo_.sampleMultiplier; ++j) {
                    outputData.writeFloat(sample);
                    outputData.writeFloat(sample);
                }
            }
        }

        private function convert8BitStereo(numSamples: uint, inputData: ByteArray, outputData: ByteArray): void
        {
            var i: uint;
            var j: uint;
            var sample: Number;

            for (i = 0; i < numSamples; ++i) {
                sample = (inputData.readUnsignedByte() - 128) / 128;

                for (j = 0; j < audioInfo_.sampleMultiplier; ++j) {
                    outputData.writeFloat(sample);
                }

                sample = (inputData.readUnsignedByte() - 128) / 128;

                for (j = 0; j < audioInfo_.sampleMultiplier; ++j) {
                    outputData.writeFloat(sample);
                }
            }
        }

        private function convert16BitStereo(numSamples: uint, inputData: ByteArray, outputData: ByteArray): void
        {
            var i: uint;
            var j: uint;
            var leftSample: Number;
            var rightSample: Number;
            var count: uint = 0;

            for (i = 0; i < numSamples; ++i) {
                leftSample = inputData.readShort() / 32767;
                rightSample = inputData.readShort() / 32767;

                for (j = 0; j < audioInfo_.sampleMultiplier; ++j) {
                    outputData.writeFloat(leftSample);
                    outputData.writeFloat(rightSample);
                    ++count;
                    ++count;
                }
            }
        }
        
        //
        //  handlers
        //
        private function dataProgressHandler(event: Event): void
        {
            if (nextMethod_ != null) {
                if (data_.bytesAvailable >= bufferSize_) {
                    isSmallFile_ = false;
                    nextMethod_();
                }
            }
        }
        
        private function dataCompleteHandler(event: Event): void
        {
            if (isSmallFile_) {
                bufferSize_ = data_.bytesAvailable;
                nextMethod_();
            }
        }
    }
}
