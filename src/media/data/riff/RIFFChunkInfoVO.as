package media.data.riff
{
    public class RIFFChunkInfoVO
    {
        public var chunkId: String;
        public var size: uint;
    }
}
