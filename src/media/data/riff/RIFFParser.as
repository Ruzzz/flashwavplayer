package media.data.riff
{
    import flash.errors.EOFError;
    import flash.events.EventDispatcher;
    import flash.events.ProgressEvent;
    import flash.utils.ByteArray;
    import flash.utils.Endian;
    import flash.utils.IDataInput;

    public class RIFFParser
    {
        private var data_: IDataInput;
        private var currentSize_: uint;
        private var chunkSize_: uint;
        private var currentPosition_: uint;
        private var startPosition_: uint;
        private var isDataComplete_: Boolean;
        private var dataSize_: uint;
        private var positionStack_: Array;
        private var sizeStack_: Array;

        //
        //  ctor
        //
        
        public function RIFFParser(data: IDataInput = null)
        {
            data_ = data;
            sizeStack_ = new Array();
            positionStack_ = new Array();
            currentPosition_ = 0;
            currentSize_ = 0;
            startPosition_ = 0;
            isDataComplete_ = false;

            if (data_ is ByteArray)
                dataSize_ = (data_ as ByteArray).length;
            else
                (data_ as EventDispatcher).addEventListener(ProgressEvent.PROGRESS, onDataProgress);
        }

        //
        //  public
        //
        
        public function get isDataComplete(): Boolean
        {
            return isDataComplete_;
        }

        public function parse(data: IDataInput): void
        {
            data_ = data;
            sizeStack_ = new Array();
            positionStack_ = new Array();
            currentPosition_ = 0;
            currentSize_ = 0;
            startPosition_ = 0;

            if (data_ is ByteArray)
                isDataComplete_ = true;
            else
                isDataComplete_ = false;
        }

        public function readChunk(): RIFFChunkInfoVO
        {
            var chunkInfo: RIFFChunkInfoVO;
            data_.endian = Endian.BIG_ENDIAN;

            if (data_.bytesAvailable >= 8) {

                // move on to next chunk
                if (currentSize_ > 0)
                    data_.readBytes(new ByteArray(), 0, currentSize_ - (currentPosition_ - startPosition_));

                // check to see if we need to pop up the stack
                while (sizeStack_.length > 0 && sizeStack_[sizeStack_.length - 1] - currentPosition_ - positionStack_[positionStack_.length - 1] < 1) {
                    sizeStack_.pop();
                    positionStack_.pop();
                }

                // if on an odd byte remove the padding byte
                if (currentPosition_ % 2 == 1) {
                    data_.readUnsignedByte();
                    ++currentPosition_;
                }

                chunkInfo = new RIFFChunkInfoVO();
                chunkInfo.chunkId = String.fromCharCode(data_.readUnsignedByte(), data_.readUnsignedByte(), data_.readUnsignedByte(), data_.readUnsignedByte());

                data_.endian = Endian.LITTLE_ENDIAN;
                chunkInfo.size = chunkSize_ = currentSize_ = data_.readUnsignedInt();

                currentPosition_ += 8;
                startPosition_ = currentPosition_;

                return chunkInfo;
            } else
                throw new EOFError("Not enough data available to read chunk");
        }

        public function readSubChunk(): String
        {
            var subChunkId: String = "";

            if (data_.bytesAvailable >= 4) {
                sizeStack_.push(currentSize_);
                positionStack_.push(startPosition_);
                currentSize_ = 0;
                startPosition_ = 0;
                data_.endian = Endian.BIG_ENDIAN;
                subChunkId = String.fromCharCode(data_.readUnsignedByte(), data_.readUnsignedByte(), data_.readUnsignedByte(), data_.readUnsignedByte());
                currentPosition_ += 4;
                return subChunkId;
            } else
                throw new EOFError("Not enough data avilable to read sub chunk");
        }

        public function readChunkData(): ByteArray
        {
            var chunkData: ByteArray;

            if (data_.bytesAvailable >= currentSize_) {
                chunkData = new ByteArray();
                data_.readBytes(chunkData, 0, currentSize_);
                currentPosition_ += currentSize_;
                currentSize_ = 0;
                return chunkData;
            } else
                throw new EOFError("Not enough data avilable to read chunk data");
        }

        public function streamChunkData(bufferSize: uint, streamData: ByteArray): uint
        {
            var readSize: int = Math.min(bufferSize, currentSize_);
            var maxPosition: uint = Math.min(chunkSize_, currentPosition_ + data_.bytesAvailable);
            var movedPosition: uint;

            readSize = Math.max(readSize, 0);

            if (data_.bytesAvailable >= readSize) {
                if (readSize > 0) {
                    data_.readBytes(streamData, streamData.length, readSize);
                    currentPosition_ += readSize;
                    currentSize_ -= readSize;
                }

                if (currentSize_ == 0 && dataSize_ > 0)
                    isDataComplete_ = true;

                return readSize;
            } else
                throw new EOFError("Not enough data avilable to read " + readSize + " bytes from chunk data");
        }

        //
        //  handlers
        //
        
        private function onDataProgress(event: ProgressEvent): void
        {
            dataSize_ = event.bytesTotal;
        }
    }
}
