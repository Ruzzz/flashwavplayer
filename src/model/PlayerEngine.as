package model
{
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.IOErrorEvent;
    import flash.events.ProgressEvent;
    import flash.events.TimerEvent;
    import flash.media.SoundChannel;
    import flash.media.SoundTransform;
    import flash.net.URLRequest;
    import flash.net.URLStream;
    import flash.utils.ByteArray;
    import flash.utils.Timer;
    
    import media.audio.AudioDecoder;
    import media.audio.decoders.WAVDecoder;
    
    import view.Player;
    import view.helpers.Position;

    public class PlayerEngine extends EventDispatcher
    {
        // Events
        static public const CAN_PLAY: String = "canPlay";
        static public const PLAYED: String = "played";
        static public const ERROR: String = "error";
        static public const PROGRESS: String = "progress";
        static public const POSITION: String = "position";
        static public const LOADED: String = "loaded";
        
        static private const BUFFER_SIZE: uint = 1048576;  // 1MB
        

        private var decoder: AudioDecoder;
        private var stream: URLStream;
        private var channel: SoundChannel;
        private var posTimer: Timer = new Timer(Player.POS_UPDATE_DELAY, 0);
        
        ///
        //  ctor
        //
        
        public function PlayerEngine()
        {
            decoder = new AudioDecoder();
            decoder.addEventListener(Event.INIT, decoderInitHandler);
            decoder.addEventListener(Event.COMPLETE, decoderCompleteHandler);
            decoder.addEventListener(IOErrorEvent.IO_ERROR, decoderIOErrorHandler);
            posTimer.addEventListener(TimerEvent.TIMER, posTimerHandler);
        }
        
        //
        //  public fields
        //
        
        private var canPlay_: Boolean;
        public function get canPlay(): Boolean { return canPlay_; }
        
        private var isActive_: Boolean;
        public function get isActive(): Boolean { return isActive_; }  // if playing or paused then true
        
        // 0..1
        private var progress_: Number;
        public function get loaded(): Number { return progress_; }
        
        // 0..1
        private var position_: Number;
        public function get position(): Number { return position_ ; }
        
        private var volume_: Number;
        public function get volume(): Number { return volume_; }
        public function set volume(value: Number): void
        {
            if (volume_ == value)
                return;
            volume_ = value;
            trace("volume", volume);
            if (channel)
                channel.soundTransform = new SoundTransform(volume_);
        }
        
        
        //
        //  public methods
        //
        
        private var url: String;
        
        public function load(fileName: String): void
        {
            canPlay_ = false;
            
            if (!fileName)
                return;
            
            stop();
            
            if (url == fileName && cache) {
                cache.position = 0;
                decoder.load(cache, WAVDecoder, BUFFER_SIZE);
                return;
            }

            trace(":: Loading: " + fileName + "\n");
            
            cache = null;
            url = fileName;
            
            freeStream();
            
            stream = new URLStream();
            stream.addEventListener(Event.COMPLETE, loadedHandler);
            stream.addEventListener(IOErrorEvent.IO_ERROR, iOErrorHandler);
            stream.addEventListener(ProgressEvent.PROGRESS, loadingHandler);
            stream.load(new URLRequest(fileName));
        }
        
        private var played_: Boolean;
        
        public function play(): void
        {
            if (played_ || !canPlay_)
                return;
            posTimer.start();
            channel = decoder.play();
            channel.soundTransform = new SoundTransform(volume_);
            isActive_ = true;
        }
        
        public function stop(): void
        {
            doStop();
            isActive_ = false;
        }
        
        public function pause(): void
        {
            doStop();
            isActive_ = true;
        }
        
        ///
        //  private methods
        //
        
        private function freeStream(): void
        {
            if (stream != null) {
                progress_ = 0;
                stream.close();
                stream.removeEventListener(Event.COMPLETE, loadedHandler);
                stream.removeEventListener(IOErrorEvent.IO_ERROR, iOErrorHandler);
                stream.removeEventListener(ProgressEvent.PROGRESS, loadingHandler);
                stream = null;
            }
        }
        
        private function doStop(): void
        {
            if (channel && isActive_ && !played_)
                channel.stop();  // isActive_ or crash (see decoderCompleteHandler) 
            posTimer.stop();
        }
        
        //
        //  handlers
        //
        
        private function posTimerHandler(e: TimerEvent): void
        {
            position_ = decoder.getPosition() / decoder.getTotalTime();
            dispatchEvent(new Event(POSITION));
        }
        
        private function loadingHandler(e: ProgressEvent): void
        {
            progress_ = e.bytesLoaded / e.bytesTotal;
            dispatchEvent(new Event(PROGRESS));
        }
        
        private var cache: ByteArray;
        
        private function loadedHandler(e: Event): void
        {
            cache = new ByteArray;
            var b: int;
            while (stream.bytesAvailable > 0) {
                b = stream.readByte();
                cache.writeByte(b);
            }
            cache.position = 0;
            decoder.load(cache, WAVDecoder, BUFFER_SIZE);
            // dispatchEvent(new Event(LOADED)); // TODO
        }
        
        private function decoderInitHandler(e: Event): void
        {
            if (!cache)
                return;
            
            trace(decoder.audioInfo.toString());
            played_ = false;
            canPlay_ = true;
            dispatchEvent(new Event(CAN_PLAY));
        }
        
        private function decoderCompleteHandler(e: Event = null): void
        {
            trace(":: Sound completed!");
            played_ = true;
            doStop();
            dispatchEvent(new Event(PLAYED));
        }

        private function iOErrorHandler(e: IOErrorEvent): void
        {
            trace(":: Error: " + e.text);
            freeStream();
            dispatchEvent(new Event(ERROR));
        }
        
        private function decoderIOErrorHandler(e: IOErrorEvent): void
        {
            trace(":: Error: " + e.text);
            doStop();
            dispatchEvent(new Event(ERROR));
        }
    }
}