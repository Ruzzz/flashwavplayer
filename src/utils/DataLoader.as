package utils
{
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.IOErrorEvent;
    import flash.net.URLLoader;
    import flash.net.URLRequest;

    [Event(name="complete", type="flash.events.Event")]
    [Event(name="complete", type="flash.events.Event")]
    public class DataLoader extends EventDispatcher
    {
        private var loader_: URLLoader;

        public function DataLoader(url: String,
                                   onCompleteCallback: Function = null,
                                   onErrorCallback: Function = null,
                                   autoLoad: Boolean = true)
        {
            url_ = url;
            onCompleteCallback_ = onCompleteCallback;
            onErrorCallback_ = onErrorCallback;
            if (autoLoad)
                load();
        }

        public function get loading(): Boolean { return loader_ != null; }

        private var data_: *;
        public function get data(): * { return data_; }

        private var url_: String;
        public function get url(): String { return url_; }
        public function set url(value: String): void { url_ = value; }

        private var onCompleteCallback_: Function;
        public function get onCompleteCallback(): Function { return onCompleteCallback_; }
        public function set onCompleteCallback(value: Function): void { onCompleteCallback_ = value; }
        
        private var onErrorCallback_: Function;
        public function get onErrorCallback(): Function { return onErrorCallback_; }
        public function set onErrorCallback(value: Function): void { onErrorCallback_ = value; }

        
        public function stop(): void
        {
            if (loader_) {
                loader_.close();
                cleanUpLoader();
            }
        }

        public function load(url: String = null): void
        {
            data_ = null;
            stop();
            loader_ = new URLLoader();
            loader_.addEventListener(Event.COMPLETE, completeHandler);
            loader_.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);  // TODO Other errors
            try {
                loader_.load(new URLRequest(url || url_));
            } catch (e: *) {
                ioErrorHandler();
            }
        }

        private function cleanUpLoader(): void
        {
            loader_.removeEventListener(Event.COMPLETE, completeHandler);
            loader_.removeEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
            loader_ = null;
        }

        private function completeHandler(e: Event): void
        {
            data_ = loader_.data;
            if (onCompleteCallback_ != null)
                onCompleteCallback_();
            else
                dispatchEvent(new Event(Event.COMPLETE));
            cleanUpLoader();
        }

        private function ioErrorHandler(e: IOErrorEvent = null): void
        {
            cleanUpLoader();
            if (onErrorCallback_ != null)
                onErrorCallback_();
        }
    }
}
