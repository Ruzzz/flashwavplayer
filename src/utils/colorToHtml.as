package utils
{
    public function colorToHtml(color: uint): String
    {
        var s: String = color.toString(16);
        while (s.length < 6)
            s = "0" + s;
        return "#" + s;
    }
}