package utils
{
    import flash.text.AntiAliasType;
    import flash.text.TextField;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormat;
    
    import flashx.textLayout.formats.TextAlign;

    public function createTextFieldLeft(x: Number, y: Number, tf: TextFormat): TextField
    {
        var label: TextField = new TextField;
        label.selectable = false;
        label.autoSize = TextFieldAutoSize.LEFT;
        //label.embedFonts = true;
        label.antiAliasType = AntiAliasType.ADVANCED;
        label.x = x;
        label.y = y;
        
        var tf_: TextFormat = new TextFormat;
        tf_.font = tf.font;
        tf_.size = tf.size;
        tf_.color = tf.color;
        tf_.bold = tf.bold;
        tf_.italic = tf.italic;
        tf_.align = TextAlign.LEFT;
        label.setTextFormat(tf_);
        label.defaultTextFormat = tf_;
        return label;
    }
}