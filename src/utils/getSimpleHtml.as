package utils
{
    import mx.utils.StringUtil;

    public function getSimpleHtml(text: String, color: uint): String
    {
        const SIMPLE_TEXT: String = "<font color='{0}'>{1}</font>";
        return StringUtil.substitute(SIMPLE_TEXT, colorToHtml(color), text || " ");  // if text is empty then flash remove general styles O_o
    }
}
