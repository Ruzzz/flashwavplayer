package utils
{
    import flash.external.ExternalInterface;

    public function jsLog(msg: String): void
    {
        if (ExternalInterface.available)
            ExternalInterface.call("console.log", msg);
    }
}