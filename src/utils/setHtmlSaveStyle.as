package utils
{
    import flash.text.TextField;
    import flash.text.TextFormat;

    public function setHtmlSaveStyle(t: TextField, html: String): void
    {
        html = html;
        var tf: TextFormat = t.getTextFormat();
        if (tf.bold)
            html = "<b>" + html + "</b>";
        if (tf.italic)
            html = "<i>" + html + "</i>";
        t.htmlText = html;
    }
}