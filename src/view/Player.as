package view
{
    import Assets.Pause;
    import Assets.Play;
    import Assets.PlayDown;
    import Assets.PlayMask;
    import Assets.PlayOver;
    import Assets.StopOver;
    
    import com.greensock.TweenLite;
    import com.ruzzz.ui.base.ButtonBase;
    import com.ruzzz.ui.base.ButtonSkinVO;
    
    import flash.display.Bitmap;
    import flash.display.DisplayObject;
    import flash.display.MovieClip;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.external.ExternalInterface;
    import flash.geom.Rectangle;
    import flash.net.SharedObject;
    import flash.text.TextFormat;
    
    import model.PlayerEngine;
    
    import utils.jsLog;
    
    import view.helpers.Position;
    import view.helpers.ScrollLine;
    import view.helpers.Volume;
    
    public class Player extends Sprite
    {
        static public const POS_UPDATE_DELAY: Number = 100;
        static private const POS_UPDATE_DELAY_SEC: Number = POS_UPDATE_DELAY / 1000;
        
        private var playerEngine: PlayerEngine = new PlayerEngine;
        private var volume: Volume;
        private var position: Position;
        private var scrollLine: ScrollLine;
        private var buttonPlay: ButtonBase;
        private var buttonStop: ButtonBase;
        private var buttonPause: ButtonBase;
        private var cookie: SharedObject;
        private var tweenPos: TweenLite;
        private var tweenLoaded: TweenLite;
        
        ///
        //  ctor
        //
        
        public function Player()
        {
            var asset: Sprite = new Assets.Player;
            volume = new Volume(asset.getChildByName("volume") as MovieClip);
            volume.addEventListener(Event.CHANGE, volumeHandler);
            position = new Position(asset.getChildByName("position") as Sprite);
            
            function setPos(obj: DisplayObject, boundsAssetName: String): void
            {
                var a: DisplayObject = asset.getChildByName(boundsAssetName);
                var rc: Rectangle = a.getRect(asset);
                asset.removeChild(a);
                obj.x = rc.x;
                obj.y = rc.y;
                asset.addChild(obj);
            };
            
            var skin: ButtonSkinVO;

            skin = new ButtonSkinVO(
                new Bitmap(new Assets.Play),
                new Bitmap(new Assets.PlayDown),
                new Assets.PlayMask,
                new Bitmap(new Assets.PlayOver));
            buttonPlay = new ButtonBase(skin, true);
            setPos(buttonPlay, "posPlay");
            buttonPlay.addEventListener(Event.CHANGE, buttonHandler);
            
            skin = new ButtonSkinVO(
                new Bitmap(new Assets.Stop),
                new Bitmap(new Assets.StopDown),
                new Assets.StopMask,
                new Bitmap(new Assets.StopOver));
            buttonStop = new ButtonBase(skin, true);
            setPos(buttonStop, "posStop");
            buttonStop.addEventListener(Event.CHANGE, buttonHandler);
            
            skin = new ButtonSkinVO(
                new Bitmap(new Assets.Pause),
                new Bitmap(new Assets.PauseDown),
                new Assets.PauseMask,
                new Bitmap(new Assets.PauseOver));
            buttonPause = new ButtonBase(skin, true);
            setPos(buttonPause, "posPause");
            buttonPause.addEventListener(Event.CHANGE, buttonHandler);
            
            addChild(asset);
            
            var tf: TextFormat = new TextFormat("Arial", 12);
            scrollLine = new ScrollLine(asset.getChildByName("boundsLabel"), tf);
            addChild(scrollLine);
            scrollLine.start();
            
            buttonStop.checked = true;
            
            playerEngine.addEventListener(PlayerEngine.CAN_PLAY, engineHandler);
            playerEngine.addEventListener(PlayerEngine.PLAYED, engineHandler);
            playerEngine.addEventListener(PlayerEngine.ERROR, engineHandler);
            playerEngine.addEventListener(PlayerEngine.PROGRESS, engineHandler);
            playerEngine.addEventListener(PlayerEngine.POSITION, engineHandler);
            playerEngine.addEventListener(PlayerEngine.LOADED, engineHandler);
            
            cookie = SharedObject.getLocal("ruzzz_wavplayer");
            if (!cookie.data.hasOwnProperty("volume"))
                cookie.data.volume = .5;
            playerEngine.volume = volume.value = cookie.data.volume;
        }
        
        ///
        //  public methods
        //
        
        private var url_: String;
        private var autoplay_: Boolean;
        
        private var firstPlay: Boolean = true;
        
        public function load(url: String, desc: String, autoplay: Boolean): void
        {
            if (!url) {
                disableAll();
                return;
            }
            
            //jsLog("desc: " + desc);
            
            url_ = url;
            autoplay_ = autoplay;
            scrollLine.setText(desc, 0x565679);
            
            //if (autoplay)
            playerEngine.load(url);
        }
        
        private function prepareUIAndPlay(): void
        {
            buttonStop.enable = true;
            buttonPause.enable = true;
            buttonPlay.enable = false;
            buttonPlay.setChecked(true);
            buttonStop.setChecked(false);
            buttonPause.setChecked(false);
            
            firstPlay = false;
            playerEngine.play();
        }
        
        private function prepareUIAndStop(): void
        {
            buttonStop.enable = false;
            buttonPause.enable = false;
            buttonPlay.enable = true;
            buttonStop.setChecked(true);
            buttonPause.setChecked(false);
            buttonPlay.setChecked(false);
            
            playerEngine.stop();
        }
        
        private function animatePlayed(value: Number): void
        {
            position.played = value;
            
            /*
            if (tweenLoaded)
                tweenLoaded.kill();
            tweenLoaded = TweenLite.to(position, POS_UPDATE_DELAY_SEC, { played : value, ease : Linear.easeNone });
            */
        }
        
        private function animateLoaded(value: Number): void
        {
            position.loaded = value;
            
            /*
            if (tweenPos)
                tweenPos.kill();
            tweenPos = TweenLite.to(position, .2, { loaded : value, ease : Linear.easeNone });  // See model.PlayerEngine.posTimer delay
            */
        }
        
        private function disableAll(): void
        {
            buttonStop.enable = false;
            buttonPause.enable = false;
            buttonPlay.enable = false;
            buttonStop.setChecked(false);
            buttonPause.setChecked(false);
            buttonPlay.setChecked(false);
        }
        
        //
        //  handlers
        //

        private function engineHandler(e: Event): void
        {
            trace("engineHandler: " + e.type);
            
            switch (e.type) {
                case PlayerEngine.CAN_PLAY:
                    if (autoplay_) {
                        prepareUIAndPlay();
                        animatePlayed(0);
                    }
                    break;
                
                case PlayerEngine.PLAYED:
                    prepareUIAndStop();
                    animatePlayed(1);
                    break;
                
                case PlayerEngine.ERROR:
                    disableAll();
                    scrollLine.setText("ОШИБКА ПРИ ЗАГРУЗКЕ ТРЕКА!!!", 0xFFFFFF);
                    break;
                
                case PlayerEngine.PROGRESS:
                    animateLoaded(playerEngine.loaded);
                    break;
                
                case PlayerEngine.POSITION:
                    animatePlayed(playerEngine.position);
                    break;
                
                case PlayerEngine.LOADED:
                    break;
            }
        }
        
        private function volumeHandler(e: Event): void
        {
            trace("Player.volumeHandler(e)");
            playerEngine.volume = cookie.data.volume = volume.value;
        }

        private function buttonHandler(e: Event): void
        {
            switch (true) {
                case (e.target == buttonPlay):
                    trace("buttonPlay");
                    
                    if (firstPlay && playerEngine.canPlay) {
                        autoplay_ = true;
                        prepareUIAndPlay();
                    } else if (!playerEngine.isActive && url_) {
                        playerEngine.load(url_);
                        autoplay_ = true;
                    } else if (playerEngine.canPlay)
                        prepareUIAndPlay();
                    break;
                
                case (e.target == buttonPause):
                    trace("buttonPause");
                    
                    buttonStop.enable = true;
                    buttonPause.enable = false;
                    buttonPlay.enable = true;
                    
                    buttonStop.setChecked(false);
                    buttonPlay.setChecked(false);
                    
                    playerEngine.pause();
                    break;
                
                case (e.target == buttonStop):
                    trace("buttonStop");
                    
                    prepareUIAndStop();
                    animatePlayed(0);
                    break;
            }
        }
    }
}