package view.helpers
{
    import flash.display.MovieClip;
    import flash.events.MouseEvent;

    public class Button
    {
        private var asset: MovieClip;
        
        public function Button(asset: MovieClip)
        {
            this.asset = asset;
            asset.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
            asset.addEventListener(MouseEvent.ROLL_OUT, mouseOutHandler);
            if (asset.totalFrames > 2)
                asset.addEventListener(MouseEvent.ROLL_OVER, mouseOverHandler);
        }
        
        private var enable_: Boolean;
        public function get enable(): Boolean { return enable_; }
        public function set enable(value: Boolean): void
        {
            if (enable_ == value)
                return;
            enable_ = value;
            asset.currentFrame(1);
        }
        
        
        private function mouseUpHandler(e: MouseEvent): void
        {
            asset.currentFrame(0);
        }
        
        private function mouseOutHandler(e: MouseEvent): void
        {
            
        }
        
        private function mouseOverHandler(e: MouseEvent): void
        {
            
        }
    }
}