package view.helpers
{
    import flash.display.DisplayObject;
    import flash.display.Sprite;

    public class Position
    {
        private var asset: Sprite;
        private var assetPlayed: DisplayObject;
        private var assetLoaded: DisplayObject;
        
        public function Position(asset: Sprite)
        {
            this.asset = asset;
            assetPlayed = asset.getChildByName("played");
            assetLoaded = asset.getChildByName("loaded");
            played = 0;
            loaded = 0;
        }
        
        private var played_: Number;
        /**
         * 0..1
         */
        public function get played(): Number { return played_; }
        public function set played(value: Number): void
        {
            if (value < 0)
                value = 0;
            if (value > 1)
                value = 1
            if (played_ == value)
                return;
            played_ = value;
            assetPlayed.scaleX = value;
        }
        
        private var loaded_: Number;
        /**
         * 0..1
         */
        public function get loaded(): Number { return loaded_; }
        public function set loaded(value: Number): void
        {
            if (value < 0)
                value = 0;
            if (value > 1)
                value = 1
            if (loaded_ == value)
                return;
            loaded_ = value;
            assetLoaded.scaleX = value;
        }
        
    }
}