package view.helpers
{
    import flash.display.DisplayObject;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.TimerEvent;
    import flash.text.TextField;
    import flash.text.TextFormat;
    import flash.utils.Timer;
    
    import utils.createTextFieldLeft;
    import utils.getSimpleHtml;
    import utils.setHtmlSaveStyle;
    
    public class ScrollLine extends Sprite
    {
        static private const PAD_LEFT: Number = 0;
        static private const STEP: Number = 1;
        
        private var padNext: Number;
        private var workWitdh: Number;
        private var scrollTexts: Vector.<TextField>;
        private var endOfRound: Boolean;
        private var timerRoundEndPause: Timer = new Timer(1000, 1);
        private var timerStartPause: Timer = new Timer(1000, 1);
        
        private var index: int = 0;
        
        private function get textMain(): TextField
        {
            return scrollTexts[index];
        }
        
        private function get textAlt(): TextField
        {
            return scrollTexts[index ^ 1];
        }
                
        private var text_: String;
        public function get text(): String
        {
            return text_;
        }
        
        private var runner_: Boolean = true;
        public function get canScroll(): Boolean
        {
            return runner_;
        }
        public function set canScroll(value: Boolean): void
        {
            if (runner_ != value) {
                runner_ = value;
                if (value)
                    checkRunnable();
                else
                    stop();
            }
        }
        
        //
        //  ctor
        //
        
        public function ScrollLine(bounds: DisplayObject, tf: TextFormat)
        {
            x = bounds.x;
            y = bounds.y;
            workWitdh = padNext = bounds.width;
            bounds.y = bounds.x = 0;
            addChild(bounds);
            mask = bounds;
            
            const H_HACK: int = -4;
            
            var scrollText: TextField = createTextFieldLeft(0, H_HACK, tf);
            var scrollTextsAlt: TextField = createTextFieldLeft(0, H_HACK, tf);
            scrollTexts = new <TextField>[scrollText, scrollTextsAlt];
            scrollTextsAlt.visible = false;
            addChild(scrollText);
            addChild(scrollTextsAlt);
            
            timerRoundEndPause.addEventListener(TimerEvent.TIMER_COMPLETE, timerStartHandler);
            timerStartPause.addEventListener(TimerEvent.TIMER_COMPLETE, timerStartHandler);
        }
        
        //
        //  public methods
        //
        
        public function start(): void
        {
            if (canScroll)
                addEventListener(Event.ENTER_FRAME, enterFrameHandler);
        }
        
        public function pause(): void
        {
            removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
        }
        
        public function stop(): void
        {
            cleaunUp();
            pause();
            textMain.x = startPos;
            textAlt.visible = false; // TODO
        }
        
        public function setText(text: String, color: uint, animate: Boolean = true): void
        {
            var html: String = getSimpleHtml(text, color);
            if (text_ == text) {
                setHtmlSaveStyle(textMain, html);
                setHtmlSaveStyle(textAlt, html);
                return;
            }

            cleaunUp();
            
            var inited: Boolean = text_ != null;
            text_ = text;
           
            setHtmlSaveStyle(textMain, html);
            setHtmlSaveStyle(textAlt, html);
            initPos();
            checkRunnable();
        }
        
        //
        //  private methods
        //
        
        private function cleaunUp(): void
        {
            timerStartPause.stop();
            timerRoundEndPause.stop();
        }
        
        private function initPos(): void
        {
            pause();
            textMain.x = startPos;
            textAlt.visible = false;
        }
        
        private function checkRunnable(): void
        {
            if (!isSmall()) {
                timerStartPause.reset();
                timerStartPause.start();
            }
        }
        
        private function isSmall(): Boolean
        {
            return textMain.width < workWitdh - PAD_LEFT;
        }
        
        private function get startPos(): Number
        {
            if (isSmall())
                return (workWitdh - textMain.width) / 2;
            else
                return PAD_LEFT;
        }
        
        private function initAlt(): void
        {
            textAlt.x = workWitdh;
            textAlt.visible = true;
            index ^= 1;
        }
        
        //
        //  Handlers
        //
        
        private function timerStartHandler(e: TimerEvent): void
        {
            start();
        }
        
        private function enterFrameHandler(e: Event): void
        {
            textMain.x -= STEP;
            if (textAlt.visible)
                textAlt.x -= STEP;
            
            if (endOfRound && textMain.x <= startPos) {
                stop();
                endOfRound = false;
                timerRoundEndPause.reset();
                timerRoundEndPause.start();
                
            } else if (!textAlt.visible && textMain.x < (workWitdh - textMain.width - padNext)) {
                
                initAlt();
                endOfRound = true;
            }
        }
    }
}