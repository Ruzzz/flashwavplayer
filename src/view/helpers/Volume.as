package view.helpers
{
    import com.ruzzz.ui.helpers.StageHandler;
    
    import flash.display.DisplayObject;
    import flash.display.MovieClip;
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;
    
    [Event(name="change", type="flash.events.Event")]
    public class Volume extends EventDispatcher
    {
        private var sh: StageHandler;
        
        private var asset: MovieClip;
        private var bounds: Rectangle;
        private var isMouseDown: Boolean = false;
        
        public function Volume(asset: MovieClip)
        {
            this.asset = asset;
            bounds = asset.getChildByName("bounds").getRect(asset);
            asset.mouseChildren = false;
            asset.mouseEnabled = true;
            asset.buttonMode = true;
            asset.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
            asset.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
            sh = new StageHandler(asset, onAddedOnStage)
            value = 0;
        }
        
        private var value_: Number;
        /**
         * 0..1
         */
        public function get value(): Number { return value_; }
        public function set value(newValue: Number): void
        {
            if (value_ == newValue)
                return;
            value_ = newValue;
            
            var frame: int = Math.round(value_ * asset.totalFrames);
            asset.gotoAndStop(frame);
        }

        private function processMouse(localX: Number): void
        {
            var x: Number = localX + 3;
            if (x < bounds.left)
                x = bounds.left;
            if (x > bounds.right)
                x = bounds.right;
            x -= bounds.left;
            x /= bounds.width;
            value = x;
            dispatchEvent(new Event(Event.CHANGE));
        }
        
        ///
        //  handlers
        //
        
        private function onAddedOnStage(): void
        {
            asset.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
        }
        
        private function mouseMoveHandler(e: MouseEvent): void
        {
            if (isMouseDown)
                processMouse(e.localX);
        }
        
        private function mouseUpHandler(e: MouseEvent): void
        {
            isMouseDown = false;
        }
        
        private function mouseDownHandler(e: MouseEvent): void
        {
            isMouseDown = true;
            processMouse(e.localX);
        }
    }
}